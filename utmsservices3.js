/**
 * Created by ckatta on 10/16/2017.
 */

var request = require('request');

var requestData = {
    "testCaseName": "test894",
    "id": 0,
    "module": {
        "project": {
            "projectId": 430,
            "projectName": "selenium",
            "projectCode": "sp1",
            "description": "selenium project",
            "webSite": "www.valuelabs.com",
            "integrationIconDisplay": false,
            "isChecked": false,
            "modulesCount": "0",
            "testcasesCount": "0",
            "autoTestcasesCount": "0",
            "configurationsCount": "0",
            "automationTypeId": 0,
            "isProjectMemberShipClone": false
        },
        "moduleName": "Selenium_Module",
        "moduleCode": "SMT",
        "moduleId": 744,
        "qcModule": false,
        "testcasesCount": "0",
        "autoTestcasesCount": "0",
        "automationTypeId": 0,
        "$$hashKey": "object:1833"
    },
    "requirement": "test",
    "testTypeVo": {
        "testTypeId": 1
    },
    "preCondition": "test",
    "testPriorityVo": {
        "testPriorityId": 3
    },
    "expectedResult": "test",
    "allStepsVOs": [{
        "isTestStep": true,
        "isDeleted": false,
        "stepNum": 1,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step1",
            "isDeleted": false
        },
        "linkedTestCase": {
            "id": 10
        },
        "$$hashKey": "object:2332"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step2",
            "isDeleted": false
        },
        "stepNum": 2,
        "$$hashKey": "object:2343"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step3",
            "isDeleted": false
        },
        "stepNum": 3,
        "$$hashKey": "object:2366"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step4",
            "isDeleted": false
        },
        "stepNum": 4,
        "$$hashKey": "object:2389"
    }]
};

request({
    method:"POST",
    url: "http://10.10.52.36:8080/utms/rest/testcase/saveOrUpdate?keys=testCaseName&value=",
    json: true,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'bearer 1d7a9884-6aa3-4dae-ab01-bf404e879100',
        'Accept': 'application/json, text/plain, */*'
    },
    body: requestData
}, function(error, response, body) {
    console.log(error);
    console.log(response.statusCode);
    console.log(body);
});