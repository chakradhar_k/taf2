/**
 * Created by ckatta on 11/20/2017.
 */

var request = require('request');

/* get all the entities in app*/
// request.get({
//   url: 'https://api.wit.ai/entities',
//   headers:{
//     'content-type':'application/json',
//     'Authorization': 'Bearer 5GDF4L24YOROLTTQJJXBT3E2RGBRX2BS'
//   },
//   json: true
// }, function(error, response, body){
//   console.log(body);
// });

/* create new entity */
// var jsonDataObj = {
//   id: "test_2",
//   doc: "test 2 description"
// };
// request.post({
//   url: 'https://api.wit.ai/entities',
//   headers:{
//     'content-type':'application/json',
//     'Authorization': 'Bearer 5GDF4L24YOROLTTQJJXBT3E2RGBRX2BS'
//   },
//   body: jsonDataObj,
//   json: true
// }, function(error, response, body){
//   console.log(body);
// });

/* get all the values of entity*/
// request.get({
//   url: 'https://api.wit.ai/entities/test',
//   headers:{
//     'content-type':'application/json',
//     'Authorization': 'Bearer 5GDF4L24YOROLTTQJJXBT3E2RGBRX2BS'
//   },
//   json: true
// }, function(error, response, body){
//   console.log(body);
// });

/* add new value to entity */
// var valueDataObj = {
//   value: "test your application"
// };
// request.post({
//   url: 'https://api.wit.ai/entities/test/values',
//   headers:{
//     'content-type':'application/json',
//     'Authorization': 'Bearer 5GDF4L24YOROLTTQJJXBT3E2RGBRX2BS'
//   },
//   body: valueDataObj,
//   json: true
// }, function(error, response, body){
//   console.log(body);
// });

/* add new expressions to entity */
var expressionDataObj = {
  expression: "CloseApp"
};
request.post({
  url: 'https://api.wit.ai/entities/CloseApp/values/CloseApp/expressions',
  headers:{
    'content-type':'application/json',
    'Authorization': 'Bearer X7FSOGAPU523OXRVGUOWZMJ4GMLZJWG7'
  },
  body: expressionDataObj,
  json: true
}, function(error, response, body){
  console.log(body);
});
