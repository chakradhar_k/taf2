/**
 * Created by ckatta on 10/16/2017.
 */

var request = require('request');

// request.get({
//   url: 'http://localhost:3000/api/RefKeywords'
// }, function (error, response, body) {
//   // console.log(body);
//   var keywords = JSON.parse(body);
//
//   console.log(keywords.length);
//   for(i = 0; i < keywords.length; i++) {
//     console.log(keywords[i]["name"]);
//   }
//
// });


var utmskeywords = {
  "AcceptPopup": {
    "FIELD2": "AcceptPopup",
    "FIELD3": "ok",
    "FIELD4": "accept",
    "FIELD5": "alert"
  },
  "Check": {
    "FIELD2": "Check",
    "FIELD3": "select",
    "FIELD4": "click"
  },
  "UnCheck": {
    "FIELD2": "UnCheck",
    "FIELD3": "click",
    "FIELD4": "select"
  },
  "ClearSession": {
    "FIELD2": "ClearSession",
    "FIELD3": "new",
    "FIELD4": "new instance",
    "FIELD5": "session clear",
    "FIELD6": "new session"
  },
  "Click": {
    "FIELD2": "Click",
    "FIELD3": "click"
  },
  "CloseBrowser": {
    "FIELD2": "CloseBrowser",
    "FIELD3": "close",
    "FIELD4": "terminate",
    "FIELD5": "close application"
  },
  "CloseDriver": {
    "FIELD2": "CloseDriver",
    "FIELD3": "close instance",
    "FIELD4": "close",
    "FIELD5": "terminate"
  },
  "Clear": {
    "FIELD2": "Clear",
    "FIELD3": "remove",
    "FIELD4": "empty"
  },
  "Enter": {
    "FIELD2": "Enter",
    "FIELD3": "click",
    "FIELD4": "next",
    "FIELD5": "move to next"
  },
  "OpenURL": {
    "FIELD2": "OpenURL",
    "FIELD3": "url",
    "FIELD4": "open",
    "FIELD5": "home",
    "FIELD6": "application"
  },
  "MouseOver": {
    "FIELD2": "MouseOver",
    "FIELD3": "over",
    "FIELD4": "above",
    "FIELD5": "place to",
    "FIELD6": "place on",
    "FIELD7": "mouse"
  },
  "GoBack": {
    "FIELD2": "GoBack",
    "FIELD3": "back",
    "FIELD4": "previous",
    "FIELD5": "last",
    "FIELD6": "last page"
  },
  "KeyPress": {
    "FIELD2": "KeyPress",
    "FIELD3": "enter",
    "FIELD4": "key press"
  },
  "SelectbyIndex": {
    "FIELD2": "SelectbyIndex",
    "FIELD3": "select",
    "FIELD4": "select first",
    "FIELD5": "select second"
  },
  "SelectbyValue": {
    "FIELD2": "SelectbyValue",
    "FIELD3": "select",
    "FIELD4": "select value",
    "FIELD5": "select by"
  },
  "SelectbyVisibleText": {
    "FIELD2": "SelectbyVisibleText",
    "FIELD3": "select",
    "FIELD4": "select value",
    "FIELD5": "select by"
  },
  "SelectRandomly": {
    "FIELD2": "SelectRandomly",
    "FIELD3": "select",
    "FIELD4": "random"
  },
  "SelectFramebyIndex": {
    "FIELD2": "SelectFramebyIndex",
    "FIELD3": "frame",
    "FIELD4": "tab",
    "FIELD5": "other"
  },
  "SelectFramebyXpath": {
    "FIELD2": "SelectFramebyXpath",
    "FIELD3": "frame",
    "FIELD4": "tab",
    "FIELD5": "other"
  },
  "Wait": {
    "FIELD2": "Wait",
    "FIELD3": "sleep",
    "FIELD4": "sla",
    "FIELD5": "time",
    "FIELD6": "duration"
  },
  "VerifyisPresent": {
    "FIELD2": "VerifyisPresent",
    "FIELD3": "exists",
    "FIELD4": "is exists",
    "FIELD5": "there"
  },
  "WaitforElement": {
    "FIELD2": "WaitforElement",
    "FIELD3": "sleep",
    "FIELD4": "sla",
    "FIELD5": "time",
    "FIELD6": "duration",
    "FIELD7": "element wait"
  },
  "DragandDrop": {
    "FIELD2": "DragandDrop",
    "FIELD3": "move",
    "FIELD4": "drag",
    "FIELD5": "drop",
    "FIELD6": "attach"
  },
  "Refresh": {
    "FIELD2": "Refresh",
    "FIELD3": "reload",
    "FIELD4": "refresh",
    "FIELD5": "open again"
  },
  "ClearCache": {
    "FIELD2": "ClearCache",
    "FIELD3": "clear the data",
    "FIELD4": "clear data",
    "FIELD5": "clean the data"
  },
  "IsDisabled": {
    "FIELD2": "IsDisabled",
    "FIELD3": "controllable",
    "FIELD4": "is diabled"
  },
  "IsEnabled": {
    "FIELD2": "IsEnabled",
    "FIELD3": "controllable",
    "FIELD4": "is enabled"
  },
  "VerifyContinue": {
    "FIELD2": "VerifyContinue"
  },
  "VerifyLink": {
    "FIELD2": "VerifyLink"
  },
  "VerifyText": {
    "FIELD2": "VerifyText"
  },
  "ClickandWait": {
    "FIELD2": "ClickandWait"
  },
  "ClearEnter": {
    "FIELD2": "ClearEnter",
    "FIELD3": "clear"
  },
  "StoreValue": {
    "FIELD2": "StoreValue"
  },
  "VerifyNotPresent": {
    "FIELD2": "VerifyNotPresent"
  },
  "VerifyisContains": {
    "FIELD2": "VerifyisContains"
  },
  "VerifyisnotContains": {
    "FIELD2": "VerifyisnotContains"
  },
  "SelectWindowbyIndex": {
    "FIELD2": "SelectWindowbyIndex"
  },
  "SelectFramebyName": {
    "FIELD2": "SelectFramebyName"
  },
  "SelectWindowbyTitle": {
    "FIELD2": "SelectWindowbyTitle"
  },
  "CloseWindowbyTitle": {
    "FIELD2": "CloseWindowbyTitle"
  },
  "CloseWindowbyIndex": {
    "FIELD2": "CloseWindowbyIndex"
  },
  "MaximiseWindow": {
    "FIELD2": "MaximiseWindow"
  },
  "GoForward": {
    "FIELD2": "GoForward"
  },
  "AssertURL": {
    "FIELD2": "AssertURL"
  },
  "DoubleClick": {
    "FIELD2": "DoubleClick"
  },
  "DismissPopup": {
    "FIELD2": "DismissPopup"
  },
  "StoreURL": {
    "FIELD2": "StoreURL"
  },
  "ScrollUp": {
    "FIELD2": "ScrollUp",
    "FIELD3": "move up",
    "FIELD4": "scroll up",
    "FIELD5": "up",
    "FIELD6": "top",
    "FIELD7": "header"
  },
  "ScrollDown": {
    "FIELD2": "ScrollDown",
    "FIELD3": "move down",
    "FIELD4": "scroll down",
    "FIELD5": "down",
    "FIELD6": "botton",
    "FIELD7": "footer"
  },
  "DeSelectAll": {
    "FIELD2": "DeSelectAll"
  },
  "DeSelectbyIndex": {
    "FIELD2": "DeSelectbyIndex"
  },
  "DeSelectbyValue": {
    "FIELD2": "DeSelectbyValue"
  },
  "DeSelectbyVisible text": {
    "FIELD2": "DeSelectbyVisible text"
  },
  "IsMultiple": {
    "FIELD2": "IsMultiple"
  },
  "SelectAll": {
    "FIELD2": "SelectAll"
  },
  "DeleteCookiebyName": {
    "FIELD2": "DeleteCookiebyName"
  },
  "VerifyLength": {
    "FIELD2": "VerifyLength"
  },
  "Upload": {
    "FIELD2": "Upload",
    "FIELD3": "attach",
    "FIELD4": "add",
    "FIELD5": "file",
    "FIELD6": "video",
    "FIELD7": "pic",
    "FIELD8": "image",
    "FIELD9": "picture"
  },
  "LockScreen": {
    "FIELD2": "LockScreen"
  },
  "IsLocked": {
    "FIELD2": "IsLocked"
  },
  "RunAppInBackground": {
    "FIELD2": "RunAppInBackground"
  },
  "HideKeyboard": {
    "FIELD2": "HideKeyboard"
  },
  "StartActivity": {
    "FIELD2": "StartActivity"
  },
  "OpenNotifications": {
    "FIELD2": "OpenNotifications"
  },
  "IsInstalled": {
    "FIELD2": "IsInstalled"
  },
  "CloseApp": {
    "FIELD2": "CloseApp"
  },
  "Reset": {
    "FIELD2": "Reset"
  },
  "KeyEvent": {
    "FIELD2": "KeyEvent"
  },
  "TouchAction": {
    "FIELD2": "TouchAction"
  },
  "SwipeLeft": {
    "FIELD2": "SwipeLeft"
  },
  "SwipeRight": {
    "FIELD2": "SwipeRight"
  },
  "SwipeUp": {
    "FIELD2": "SwipeUp"
  },
  "SwipeDown": {
    "FIELD2": "SwipeDown"
  },
  "Pinch": {
    "FIELD2": "Pinch"
  },
  "Zoom": {
    "FIELD2": "Zoom"
  },
  "PullFile": {
    "FIELD2": "PullFile"
  },
  "PushFile": {
    "FIELD2": "PushFile"
  },
  "Rotate": {
    "FIELD2": "Rotate"
  },
  "IsNativeApp": {
    "FIELD2": "IsNativeApp"
  },
  "JsClick": {
    "FIELD2": "JsClick"
  },
  "OpenTab": {
    "FIELD2": "OpenTab"
  },
  "CloseTab": {
    "FIELD2": "CloseTab"
  },
  "SwitchToDefaultFrame": {
    "FIELD2": "SwitchToDefaultFrame"
  },
  "VerifyColor": {
    "FIELD2": "VerifyColor"
  },
  "VerifyFontSize": {
    "FIELD2": "VerifyFontSize"
  },
  "VerifyFontWeight": {
    "FIELD2": "VerifyFontWeight"
  },
  "VerifyFontFamily": {
    "FIELD2": "VerifyFontFamily"
  },
  "VerifyFontStyle": {
    "FIELD2": "VerifyFontStyle"
  },
  "VerifyTextAlignment": {
    "FIELD2": "VerifyTextAlignment"
  },
  "VerifyBackGroundColor": {
    "FIELD2": "VerifyBackGroundColor"
  },
  "VerifyTextTransformation": {
    "FIELD2": "VerifyTextTransformation"
  },
  "VerifyTextDecoration": {
    "FIELD2": "VerifyTextDecoration"
  },
  "GetColor": {
    "FIELD2": "GetColor"
  },
  "GetFontSize": {
    "FIELD2": "GetFontSize"
  },
  "GetFontWeight": {
    "FIELD2": "GetFontWeight"
  },
  "GetFontFamily": {
    "FIELD2": "GetFontFamily"
  },
  "GetFontStyle": {
    "FIELD2": "GetFontStyle"
  },
  "GetTextAlignment": {
    "FIELD2": "GetTextAlignment"
  },
  "GetBackGroundColor": {
    "FIELD2": "GetBackGroundColor"
  },
  "GetTextTransformation": {
    "FIELD2": "GetTextTransformation"
  },
  "GetTextDecoration": {
    "FIELD2": "GetTextDecoration"
  },
  "VerifyisContainsIgnoreCase": {
    "FIELD2": "VerifyisContainsIgnoreCase"
  },
  "VerifyisnotContainsIgnoreCase": {
    "FIELD2": "VerifyisnotContainsIgnoreCase"
  }
};

for(var key in utmskeywords) {
  // console.log(key, "----------");

  var jsonDataObj = {
    "doc": "User-defined entity",
    "id": key,
    "values": [
      {
        "value": key,
        "expressions": []
      }
    ]
  };

  for(var key2 in utmskeywords[key]) {
    // console.log(utmskeywords[key][key2]);
    jsonDataObj.values[0].expressions.push(utmskeywords[key][key2]);
  }

  console.log(JSON.stringify(jsonDataObj));

  request.delete({
    url: 'https://api.wit.ai/entities/'+key,
    headers:{
          'content-type':'application/json',
          'Authorization': 'Bearer 7OD2HBRMVYYJR6XEXDHMICQKZTUBLEMV'
        },
  });
  // request.put({
  //   url: 'https://api.wit.ai/entities',
  //   headers:{
  //     'content-type':'application/json',
  //     'Authorization': 'Bearer 7OD2HBRMVYYJR6XEXDHMICQKZTUBLEMV'
  //   },
  //   body: jsonDataObj,
  //   json: true
  // }, function(error, response, body){
  //   console.log(body);
  // });

}

// var jsonDataObj = {
//   "doc": "A city that I like",
//   "id": "favorite_city2",
//   "values": [
//     {
//       "value": "Paris",
//       "expressions": [
//         "Paris",
//         "City of Light",
//         "Capital of France"
//       ]
//     }
//   ]
// };
//
// request.post({
//   url: 'https://api.wit.ai/entities',
//   headers:{
//     'content-type':'application/json',
//     'Authorization': 'Bearer 7OD2HBRMVYYJR6XEXDHMICQKZTUBLEMV'
//   },
//   body: jsonDataObj,
//   json: true
// }, function(error, response, body){
//   console.log(body);
// });
