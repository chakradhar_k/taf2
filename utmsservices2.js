/**
 * Created by ckatta on 10/16/2017.
 */

var http = require('http');

var querystring = require('querystring');

var testcaseData = {
    "testCaseName": "test891",
    "id": 0,
    "module": {
        "project": {
            "projectId": 430,
            "projectName": "selenium",
            "projectCode": "sp1",
            "description": "selenium project",
            "webSite": "www.valuelabs.com",
            "integrationIconDisplay": false,
            "isChecked": false,
            "modulesCount": "0",
            "testcasesCount": "0",
            "autoTestcasesCount": "0",
            "configurationsCount": "0",
            "automationTypeId": 0,
            "isProjectMemberShipClone": false
        },
        "moduleName": "Selenium_Module",
        "moduleCode": "SMT",
        "moduleId": 744,
        "qcModule": false,
        "testcasesCount": "0",
        "autoTestcasesCount": "0",
        "automationTypeId": 0,
        "$$hashKey": "object:1833"
    },
    "requirement": "test",
    "testTypeVo": {
        "testTypeId": 1
    },
    "preCondition": "test",
    "testPriorityVo": {
        "testPriorityId": 3
    },
    "expectedResult": "test",
    "allStepsVOs": [{
        "isTestStep": true,
        "isDeleted": false,
        "stepNum": 1,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step1",
            "isDeleted": false
        },
        "linkedTestCase": {
            "id": 10
        },
        "$$hashKey": "object:2332"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step2",
            "isDeleted": false
        },
        "stepNum": 2,
        "$$hashKey": "object:2343"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step3",
            "isDeleted": false
        },
        "stepNum": 3,
        "$$hashKey": "object:2366"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step4",
            "isDeleted": false
        },
        "stepNum": 4,
        "$$hashKey": "object:2389"
    }]
};

const postData = querystring.stringify({
    "testCaseName": "test891",
    "id": 0,
    "module": {
        "project": {
            "projectId": 430,
            "projectName": "selenium",
            "projectCode": "sp1",
            "description": "selenium project",
            "webSite": "www.valuelabs.com",
            "integrationIconDisplay": false,
            "isChecked": false,
            "modulesCount": "0",
            "testcasesCount": "0",
            "autoTestcasesCount": "0",
            "configurationsCount": "0",
            "automationTypeId": 0,
            "isProjectMemberShipClone": false
        },
        "moduleName": "Selenium_Module",
        "moduleCode": "SMT",
        "moduleId": 744,
        "qcModule": false,
        "testcasesCount": "0",
        "autoTestcasesCount": "0",
        "automationTypeId": 0,
        "$$hashKey": "object:1833"
    },
    "requirement": "test",
    "testTypeVo": {
        "testTypeId": 1
    },
    "preCondition": "test",
    "testPriorityVo": {
        "testPriorityId": 3
    },
    "expectedResult": "test",
    "allStepsVOs": [{
        "isTestStep": true,
        "isDeleted": false,
        "stepNum": 1,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step1",
            "isDeleted": false
        },
        "linkedTestCase": {
            "id": 10
        },
        "$$hashKey": "object:2332"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step2",
            "isDeleted": false
        },
        "stepNum": 2,
        "$$hashKey": "object:2343"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step3",
            "isDeleted": false
        },
        "stepNum": 3,
        "$$hashKey": "object:2366"
    }, {
        "isTestStep": true,
        "isDeleted": false,
        "testStepVO": {
            "id": 0,
            "stepDescription": "step4",
            "isDeleted": false
        },
        "stepNum": 4,
        "$$hashKey": "object:2389"
    }]
});

const options = {
    hostname: '10.10.52.36',
    port: 8080,
    path: '/utms/rest/testcase/saveOrUpdate?keys=testCaseName&value=',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'bearer b6d0adfb-4efc-4f21-a137-707425edb1a7'
    }
};

const req = http.request(options, function (res) {
        console.log(`STATUS: ${res.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log(`BODY: ${chunk}`);
        });
        res.on('end', function () {
            console.log('No more data in response.');
        });
});

req.on('error', function (e)  {
    console.error(`problem with request: ${e.message}`);
});

// write data to request body
req.write(postData);
req.end();