/**
 * Created by ckatta on 10/21/2017.
 */

var txtgen = require('txtgen');

let sentence = txtgen.sentence();
console.log(sentence);

let paragraph = txtgen.paragraph();
console.log(paragraph);

let article = txtgen.article();
console.log(article);
