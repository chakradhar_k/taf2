/**
 * Created by ckatta on 10/16/2017.
 */

var http = require('http');
var request = require('request');

var querystring = require('querystring');

const options = {
    hostname: '10.10.52.36',
    port: 8080,
    path: '/utms/oauth/token?username=userunited@vl.com&password=userunited@vl.com&grant_type=password',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
    }
};

var accessTokenInformation
const req = http.request(options, function (res) {
        // console.log(`STATUS: ${res.statusCode}`);
        // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding('utf8');
        res.on('data', function (chunk) {

            accessTokenInformation = chunk;
            // console.log(accessTokenInformation);
            // console.log(`BODY: ${chunk}`);
        });
        res.on('end', function () {
            console.log("body: "+accessTokenInformation);
            accessTokenInformation = JSON.parse(accessTokenInformation);

            /* to get all the testcases*/
            // request({
            //   method:"GET",
            //   url: "http://10.10.52.36:8080/utms/rest/automation/viewAutomation/1593/0/40/{}/2?keys=autoTestCaseName&value=",
            //   json: true,
            //   headers: {
            //     'Content-Type': 'application/json',
            //     'Authorization': 'bearer '+ accessTokenInformation['access_token'],
            //     'Accept': 'application/json, text/plain, */*'
            //   }
            // }, function(error, response, body) {
            //   console.log(error);
            //   console.log(response.statusCode);
            //   console.log(body);
            // });

          /* to get manual test steps*/
          // request({
          //   method:"GET",
          //   url: "http://10.10.52.36:8080/utms/rest/automation/allTeststepDetails/81891?keys=autoTestCaseName&value=",
          //   json: true,
          //   headers: {
          //     'Content-Type': 'application/json',
          //     'Authorization': 'bearer '+ accessTokenInformation['access_token'],
          //     'Accept': 'application/json, text/plain, */*'
          //   }
          // }, function(error, response, body) {
          //   console.log(error);
          //   console.log(response.statusCode);
          //   console.log(body);
          // });

          /*to get automated test steps*/
          request({
            method:"GET",
            url: "http://10.10.52.36:8080/utms/rest/automation/getAutoTestStepDetails/10271?keys=autoTestCaseName&value=",
            json: true,
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'bearer '+ accessTokenInformation['access_token'],
              'Accept': 'application/json, text/plain, */*'
            }
          }, function(error, response, body) {
            console.log(error);
            console.log(response.statusCode);
            console.log(body);
          });

            // console.log('No more data in response.');
        });
});

req.on('error', function (e)  {
    console.error(`problem with request: ${e.message}`);
});

req.end();
