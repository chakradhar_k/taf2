/**
 * Created by ckatta on 10/16/2017.
 */

var http = require('http');

var querystring = require('querystring');

const postData = querystring.stringify({
  "doc": "A city that I like",
  "id": "favorite_city2",
  "values": [
    {
      "value": "Paris",
      "expressions": [
        "Paris",
        "City of Light",
        "Capital of France"
      ]
    }
  ]
});

const options = {
    hostname: 'api.wit.ai',
    port: 443,
    path: '/entities',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer 7OD2HBRMVYYJR6XEXDHMICQKZTUBLEMV'
    }
};

const req = http.request(options, function (res) {
        console.log(`STATUS: ${res.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log(`BODY: ${chunk}`);
        });
        res.on('end', function () {
            console.log('No more data in response.');
        });
});

req.on('error', function (e)  {
  console.log(e);
    console.error(`problem with request: ${e.message}`);
});

// write data to request body
req.write(postData);
req.end();
