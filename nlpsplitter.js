/**
 * Created by ckatta on 10/17/2017.
 */

var openNLP = require("opennlp");
var sentence = 'Pierre Vinken , 61 years old , will join the board as a nonexecutive director Nov. 29 .';
var sentenceDetector = new openNLP().sentenceDetector;
sentenceDetector.sentDetect(sentence, function(err, results) {
  /// To get probabilities
  sentenceDetector.probs(function(error,probability){
    console.log(error,probability)
  })
  console.log(results)
});
