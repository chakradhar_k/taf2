'use strict';
var natural = require('natural');
var changeCase = require('change-case');
const {Wit, log} = require('node-wit');
var wordnet = new natural.WordNet();

module.exports = function (Xpathcreator) {

  Xpathcreator.observe('before save', function (ctx, next) {

    if (ctx.instance) {

      var tokenizer = new natural.WordTokenizer();
      var stringTokens = tokenizer.tokenize(ctx.instance.text);

      var finalXPath = "";
      var finalElement = "";
      ctx.instance["objectName"] = changeCase.constantCase(ctx.instance.text);

      const client = new Wit({accessToken: 'SELBDAKRC3TUOFKA33JHBUQECC2POFDE'});
      client.message(ctx.instance.text, {}).then(function (data) {
        console.log('Yay, got Wit.ai response: ' + JSON.stringify(data));
        ctx.instance["automatedTestStep"] = data;

        stringTokens.forEach(function (stringToken) {

          wordnet.lookup(stringToken, function(results) {
            // console.log(results);
            results.forEach(function(result) {
              console.log('------------------------------------');
              // console.log(result.synsetOffset);
              // console.log(result.pos);
              // console.log(result.lemma);
              console.log(result.synonyms);
              // console.log(result.pos);
              // console.log(result.gloss);
            });
          });

          var xpathTemplate = "//*[not(self::script)][contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'" + stringToken.toLocaleLowerCase() + "')]";

          if (finalXPath != "") {
            finalXPath = finalXPath + "|" + xpathTemplate;
            finalElement = finalElement + " or translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"+stringToken.toLocaleLowerCase()+"'";
            finalElement = finalElement + " or translate(@id,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"+stringToken.toLocaleLowerCase()+"'";
            finalElement = finalElement + " or translate(@class,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"+stringToken.toLocaleLowerCase()+"'";
          } else {
            finalXPath = xpathTemplate;
            finalElement = finalElement + "translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"+stringToken.toLocaleLowerCase()+"'";
            finalElement = finalElement + " or translate(@id,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"+stringToken.toLocaleLowerCase()+"'";
            finalElement = finalElement + " or translate(@class,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"+stringToken.toLocaleLowerCase()+"'";
          }

        })

        var utmsKey = "";

        console.log("----------entities--------------");
        // console.log(ctx.instance.automatedTestStep.entities);
        if(ctx.instance.automatedTestStep.entities.length != undefined) {

        } else {
           for(var key in ctx.instance.automatedTestStep.entities){
             utmsKey = key;
             break;
           }
        }

        if(key=="Enter") {

          finalXPath = "(" + finalXPath + ")/following::input[not(@type='hidden')]["+finalElement+"][1]";

        }

        ctx.instance["generatedXPath"] = finalXPath;
        next();
      }).catch(console.error);

    }

  });

};
