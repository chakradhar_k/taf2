'use strict';

module.exports = function(Testcase) {

  Testcase.observe('after save', function(ctx, next) {
    if(ctx.instance) {
      console.log(ctx.instance);
      var request = require('request');

      var requestData= ctx.instance;

      request({
        method:"POST",
        url: "http://10.10.52.36:8080/utms/rest/testcase/saveOrUpdate?keys=testCaseName&value=",
        json: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer 1d7a9884-6aa3-4dae-ab01-bf404e879100',
          'Accept': 'application/json, text/plain, */*'
        },
        body: requestData
      }, function(error, response, body) {
        console.log(error);
        console.log(response.statusCode);
        console.log(body);
      });
      next();
    } else {

    }
  });

  Testcase.getManualTestSteps = function(testcaseId, cb) {

    /**
     * Created by ckatta on 10/16/2017.
     */

    var http = require('http');
    var request = require('request');

    var querystring = require('querystring');

    const options = {
      hostname: '10.10.52.36',
      port: 8080,
      path: '/utms/oauth/token?username=userunited@vl.com&password=userunited@vl.com&grant_type=password',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
      }
    };

    var accessTokenInformation
    const req = http.request(options, function (res) {
      // console.log(`STATUS: ${res.statusCode}`);
      // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
      res.setEncoding('utf8');
      res.on('data', function (chunk) {

        accessTokenInformation = chunk;
        // console.log(accessTokenInformation);
        // console.log(`BODY: ${chunk}`);
      });
      res.on('end', function () {
        console.log("body: "+accessTokenInformation);
        accessTokenInformation = JSON.parse(accessTokenInformation);

        /* to get manual test steps*/
        request({
          method:"GET",
          url: "http://10.10.52.36:8080/utms/rest/automation/allTeststepDetails/"+testcaseId+"?keys=autoTestCaseName&value=",
          json: true,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+ accessTokenInformation['access_token'],
            'Accept': 'application/json, text/plain, */*'
          }
        }, function(error, response, body) {
          console.log(error);
          console.log(response.statusCode);
          console.log(body);
          cb(null, body);
        });

        // console.log('No more data in response.');
      });
    });

    req.on('error', function (e)  {
      console.error(`problem with request: ${e.message}`);
    });

    req.end();

  }

  Testcase.remoteMethod('getManualTestSteps', {
    http: {path: '/getManualTestSteps', verb: 'get'},
    accepts: {arg: 'testcaseId', type: 'string'},
    returns: {arg: 'teststeps', type: 'JSON'}
  });

  Testcase.getAutomatedTestSteps = function(cb) {

    /**
     * Created by ckatta on 10/16/2017.
     */

    var http = require('http');
    var request = require('request');

    var querystring = require('querystring');

    const options = {
      hostname: '10.10.52.36',
      port: 8080,
      path: '/utms/oauth/token?username=userunited@vl.com&password=userunited@vl.com&grant_type=password',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
      }
    };

    var accessTokenInformation
    const req = http.request(options, function (res) {
      // console.log(`STATUS: ${res.statusCode}`);
      // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
      res.setEncoding('utf8');
      res.on('data', function (chunk) {

        accessTokenInformation = chunk;
        // console.log(accessTokenInformation);
        // console.log(`BODY: ${chunk}`);
      });
      res.on('end', function () {
        console.log("body: "+accessTokenInformation);
        accessTokenInformation = JSON.parse(accessTokenInformation);

        /*to get automated test steps*/
        request({
          method:"GET",
          url: "http://10.10.52.36:8080/utms/rest/automation/getAutoTestStepDetails/10341?keys=autoTestCaseName&value=",
          json: true,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+ accessTokenInformation['access_token'],
            'Accept': 'application/json, text/plain, */*'
          }
        }, function(error, response, body) {
          console.log(error);
          console.log(response.statusCode);
          console.log(body);
          cb(null, body);
        });

        // console.log('No more data in response.');
      });
    });

    req.on('error', function (e)  {
      console.error(`problem with request: ${e.message}`);
    });

    req.end();

  }

  Testcase.remoteMethod('getAutomatedTestSteps', {
    http: {path: '/getAutomatedTestSteps', verb: 'get'},
    returns: {arg: 'teststeps', type: 'JSON'}
  });

};
