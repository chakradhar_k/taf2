'use strict';

var changeCase = require('change-case');

var server = require('../../server/server');

module.exports = function(Automatedtestcasegenerator) {

  Automatedtestcasegenerator.observe('after save', function(ctx, next) {

    if(ctx.instance) {

      var AutomatedTestcase = server.models.AutomatedTestcase;

      var automatedTestcase = {};

      automatedTestcase["TestCaseName"] = changeCase.snakeCase(ctx.instance['TestCaseName']);
      automatedTestcase["manualTestcaseLink"] = ctx.instance.id;

      AutomatedTestcase.create(automatedTestcase, function(err, aTestcase){

        if(err) {

        } else {

        }
        next();

      });


    } else {
      next();
    }


  });

};
