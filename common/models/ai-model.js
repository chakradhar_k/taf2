'use strict';

var request = require('request');
var appAuthorizationId = "X54XHNMOLZWE2WL3OJNJSKBKUHXRCNCK";

module.exports = function(Aimodel) {
  Aimodel.observe('before save', function(ctx, next){

    if(ctx.instance) {

      console.log(appAuthorizationId);
      var expressionDataObj = {
        expression: ctx.instance.expression
      };
      request.post({
        url: 'https://api.wit.ai/entities/'+ctx.instance.keyword+'/values/'+ctx.instance.keyword+'/expressions',
        headers:{
          'content-type':'application/json',
          'Authorization': 'Bearer '+ appAuthorizationId
        },
        body: expressionDataObj,
        json: true
      }, function(error3, response3, body3){
        ctx.instance["response"] = body3;
        next();
      });

    }

  });
};
