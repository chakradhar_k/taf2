'use strict';

module.exports = function(Stestcase) {

  Stestcase.observe('after save', function(ctx, next) {

    var loopback = require('loopback');
    var ds = loopback.createDataSource('mysql', {
      "host": "10.10.53.214",
      "port": 3306,
      "database": "utmsdb",
      "username": "root",
      "password": "root"
    });

    // console.log(ds);

    console.log(ds.discoverModelDefinitions());

    next();

  });

};
