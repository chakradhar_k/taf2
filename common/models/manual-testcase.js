'use strict';

var changeCase = require('change-case');

module.exports = function(Manualtestcase) {

  Manualtestcase.observe('after save', function(ctx, next) {

    if(ctx.instance) {

      var automatedTestcase = {};

      automatedTestcase["TestCaseName"] = changeCase.snakeCase(ctx.instance['TestCaseName']);
      automatedTestcase["manualIdLink"] = ctx.instance.id;


    } else {

    }
    next();

  });

};
