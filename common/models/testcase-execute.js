'use strict';

var http = require('http');
var request = require('request');

var querystring = require('querystring');

module.exports = function(Testcaseexecute) {

  Testcaseexecute.observe('after save', function(ctx, next){

    if(ctx.instance) {

      const options = {
        hostname: '10.10.52.36',
        port: 8080,
        path: '/utms/oauth/token?username=userunited@vl.com&password=userunited@vl.com&grant_type=password',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic MzUzYjMwMmM0NDU3NGY1NjUwNDU2ODdlNTM0ZTdkNmE6Mjg2OTI0Njk3ZTYxNWE2NzJhNjQ2YTQ5MzU0NTY0NmM='
        }
      };

      var accessTokenInformation
      const req = http.request(options, function (res) {
        // console.log(`STATUS: ${res.statusCode}`);
        // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding('utf8');
        res.on('data', function (chunk) {

          accessTokenInformation = chunk;
          // console.log(accessTokenInformation);
          // console.log(`BODY: ${chunk}`);
        });
        res.on('end', function () {
          console.log("body: "+accessTokenInformation);
          accessTokenInformation = JSON.parse(accessTokenInformation);
          var requestData = {
            "hubId": 1156,
            "projectId": 793,
            "configId": 5802,
            "environemntId": 2,
            "userId": 1531,
            "hubIp": "172.25.82.46",
            "hubPort": "14444",
            "operatingSystemName": "Windows 8",
            "storeResults": true,
            "browsers": [{
              "refOsRefBrowserId": 45,
              "browserVersion": "47",
              "browserName": "Chrome"
            }],
            "label": "chakri"
          }

          request({
            method:"POST",
            url: "http://10.10.52.36:8080/utms/rest/exeConfig/executeLocally?companyId=371&projectId=793",
            json: true,
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'bearer '+ accessTokenInformation['access_token'],
              'Accept': 'application/json, text/plain, */*'
            },
            body: requestData
          }, function(error, response, body) {
            console.log(error);
            console.log(response.statusCode);
            console.log(body);
            next();
          });

          // console.log('No more data in response.');
        });
      });

      req.on('error', function (e)  {
        console.error(`problem with request: ${e.message}`);
      });

      req.end();

    } else {
      next();
    }

  })

};
