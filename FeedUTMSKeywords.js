/**
 * Created by ckatta on 11/20/2017.
 */

var request = require('request');

var appAuthorizationId = "X54XHNMOLZWE2WL3OJNJSKBKUHXRCNCK";

var utmskeywords =[
  ["Check","select","click","click the button","","","",""  ],
  ["UnCheck","click","select","","","","",""  ],
  ["ClearSession","new","new instance","session clear","new session","","",""  ],
  ["Click","click","","","","","",""  ],
  ["CloseBrowser","close","terminate","close application","","","",""  ],
  ["CloseDriver","close instance","close","terminate","","","",""  ],
  ["Clear","remove","empty","","","","",""  ],
  ["Enter","click","next","move to next","","","",""  ],
  ["OpenURL","url","open","home","application","","",""  ],
  ["MouseOver","over","above","place to","place on","mouse","",""  ],
  ["GoBack","back","previous","last","last page","","",""  ],
  ["KeyPress","enter","key press","","","","",""  ],
  ["SelectbyIndex","select","select first","select second","","","",""  ],
  ["SelectbyValue","select","select value","select by","","","",""  ],
  ["SelectbyVisibleText","select","select value","select by","","","",""  ],
  ["SelectRandomly","select","random","","","","",""  ],
  ["SelectFramebyIndex","frame","tab","other","","","",""  ],
  ["SelectFramebyXpath","frame","tab","other","","","",""  ],
  ["Wait","sleep","sla","time","duration","","",""  ],
  ["VerifyisPresent","exists","is exists","there","","","",""  ],
  ["WaitforElement","sleep","sla","time","duration","element wait","",""  ],
  ["DragandDrop","move","drag","drop","attach","","",""  ],
  ["Refresh","reload","refresh","open again","","","",""  ],
  ["ClearCache","clear the data","clear data","clean the data","","","",""  ],
  ["IsDisabled","controllable","is diabled","","","","",""  ],
  ["IsEnabled","controllable","is enabled","","","","",""  ],
  ["VerifyContinue","","","","","","",""  ],
  ["VerifyLink","","","","","","",""  ],
  ["VerifyText","","","","","","",""  ],
  ["ClickandWait","","","","","","",""  ],
  ["ClearEnter","clear","","","","","",""  ],
  ["StoreValue","","","","","","",""  ],
  ["VerifyNotPresent","","","","","","",""  ],
  ["VerifyisContains","","","","","","",""  ],
  ["VerifyisnotContains","","","","","","",""  ],
  ["SelectWindowbyIndex","","","","","","",""  ],
  ["SelectFramebyName","","","","","","",""  ],
  ["SelectWindowbyTitle","","","","","","",""  ],
  ["CloseWindowbyTitle","","","","","","",""  ],
  ["CloseWindowbyIndex","","","","","","",""  ],
  ["MaximiseWindow","","","","","","",""  ],
  ["GoForward","","","","","","",""  ],
  ["AssertURL","","","","","","",""  ],
  ["DoubleClick","","","","","","",""  ],
  ["DismissPopup","","","","","","",""  ],
  ["StoreURL","","","","","","",""  ],
  ["ScrollUp","move up","scroll up","up","top","header","",""  ],
  ["ScrollDown","move down","scroll down","down","botton","footer","",""  ],
  ["DeSelectAll","","","","","","",""  ],
  ["DeSelectbyIndex","","","","","","",""  ],
  ["DeSelectbyValue","","","","","","",""  ],
  ["DeSelectbyVisible text","","","","","","",""  ],
  ["IsMultiple","","","","","","",""  ],
  ["SelectAll","","","","","","",""  ],
  ["DeleteCookiebyName","","","","","","",""  ],
  ["VerifyLength","","","","","","",""  ],
  ["Upload","attach","add","file","video","pic","image","picture"  ],
  ["LockScreen","","","","","","",""  ],
  ["IsLocked","","","","","","",""  ],
  ["RunAppInBackground","","","","","","",""  ],
  ["HideKeyboard","","","","","","",""  ],
  ["StartActivity","","","","","","",""  ],
  ["OpenNotifications","","","","","","",""  ],
  ["IsInstalled","","","","","","",""  ],
  ["CloseApp","","","","","","",""  ],
  ["Reset","","","","","","",""  ],
  ["KeyEvent","","","","","","",""  ],
  ["TouchAction","","","","","","",""  ],
  ["SwipeLeft","","","","","","",""  ],
  ["SwipeRight","","","","","","",""  ],
  ["SwipeUp","","","","","","",""  ],
  ["SwipeDown","","","","","","",""  ],
  ["Pinch","","","","","","",""  ],
  ["Zoom","","","","","","",""  ],
  ["PullFile","","","","","","",""  ],
  ["PushFile","","","","","","",""  ],
  ["Rotate","","","","","","",""  ],
  ["IsNativeApp","","","","","","",""  ],
  ["JsClick","","","","","","",""  ],
  ["OpenTab","","","","","","",""  ],
  ["CloseTab","","","","","","",""  ],
  ["SwitchToDefaultFrame","","","","","","",""  ],
  ["VerifyColor","","","","","","",""  ],
  ["VerifyFontSize","","","","","","",""  ],
  ["VerifyFontWeight","","","","","","",""  ],
  ["VerifyFontFamily","","","","","","",""  ],
  ["VerifyFontStyle","","","","","","",""  ],
  ["VerifyTextAlignment","","","","","","",""  ],
  ["VerifyBackGroundColor","","","","","","",""  ],
  ["VerifyTextTransformation","","","","","","",""  ],
  ["VerifyTextDecoration","","","","","","",""  ],
  ["GetColor","","","","","","",""  ],
  ["GetFontSize","","","","","","",""  ],
  ["GetFontWeight","","","","","","",""  ],
  ["GetFontFamily","","","","","","",""  ],
  ["GetFontStyle","","","","","","",""  ],
  ["GetTextAlignment","","","","","","",""  ],
  ["GetBackGroundColor","","","","","","",""  ],
  ["GetTextTransformation","","","","","","",""  ],
  ["GetTextDecoration","","","","","","",""  ],
  ["VerifyisContainsIgnoreCase","","","","","","",""  ],
  ["VerifyisnotContainsIgnoreCase","","","","","","",""  ]
]

utmskeywords.forEach(function(singleKeyword) {
  // console.log(key, "----------");

  /* create new entity */
  var jsonDataObj = {
    id: singleKeyword[0],
    doc: "User-defined entity",
    lookups: ["trait"]
  };

  request.post({
    url: 'https://api.wit.ai/entities',
    headers: {
      'content-type': 'application/json',
      'Authorization': 'Bearer '+ appAuthorizationId
    },
    body: jsonDataObj,
    json: true
  }, function (error, response, body) {
    // console.log(body);

    /*add the value for created entity*/
    var valueDataObj = {
      value: singleKeyword[0]
    };
    request.post({
      url: 'https://api.wit.ai/entities/'+ singleKeyword[0] +'/values',
      headers: {
        'content-type': 'application/json',
        'Authorization': 'Bearer ' + appAuthorizationId
      },
      body: valueDataObj,
      json: true
    }, function (error2, response2, body2) {
      // console.log(body2);

      singleKeyword.forEach(function(keyExpression) {

        if(keyExpression != "") {
          console.log(singleKeyword[0]+"------ "+keyExpression);
          /* add new expressions to entity and value */
          var expressionDataObj = {
            expression: keyExpression
          };
          request.post({
            url: 'https://api.wit.ai/entities/'+singleKeyword[0]+'/values/'+singleKeyword[0]+'/expressions',
            headers:{
              'content-type':'application/json',
              'Authorization': 'Bearer '+ appAuthorizationId
            },
            body: expressionDataObj,
            json: true
          }, function(error3, response3, body3){
            console.log(body3);
          });
        }

      });

    });

  });

});
